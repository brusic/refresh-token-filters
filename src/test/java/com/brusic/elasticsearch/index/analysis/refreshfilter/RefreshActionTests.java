package com.brusic.elasticsearch.index.analysis.refreshfilter;

import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterAction;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterRequest;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterResponse;
import com.brusic.elasticsearch.action.refreshfilter.TransportRefreshFilterAction;
import com.google.common.collect.Lists;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeRequest;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.Priority;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.elasticsearch.common.settings.ImmutableSettings.settingsBuilder;
import static org.elasticsearch.node.NodeBuilder.nodeBuilder;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class RefreshActionTests {

    private Node node;

    @BeforeClass
    protected void setupServer() {
        node = nodeBuilder().local(true).settings(settingsBuilder()
                .put("cluster.name", "test-refresh")
                .put("node.name", "refresh-node")
                .put("gateway.type", "none")
                .put("index.numberOfReplicas", 0)
                .put("index.numberOfShards", 1)
        ).node();
    }

    @Test
    public void testSynonymRefresh() throws IOException {
        File synonymsFile = new File("src/test/resources/com/brusic/elasticsearch/index/analysis/refreshfilter/synonyms.txt");
        writeFile(synonymsFile, Lists.<String>newArrayList("quick => fast", "dog => canine"));

        Settings settings = settingsBuilder().loadFromClasspath("com/brusic/elasticsearch/index/analysis/refreshfilter/synonyms.json").build();
        createIndex("test", settings);

        String text = "the quick brown fox jumped over the lazy dog";

        // Before the refresh

        String[] tokens = {"the", "fast", "brown", "fox", "jumped", "over", "the", "lazy", "canine"};
        //assertTokenStream(tokenStream, tokens);

        // After the refresh
        writeFile(synonymsFile, Lists.<String>newArrayList("quick => fastest", "lazy => sleepy"));

        RefreshFilterRequest refreshFilterRequest = new RefreshFilterRequest();
        refreshFilterRequest.indices("test");
        refreshFilterRequest.tokenFilter("synonym_file");

        RefreshFilterResponse response = node.client().admin().cluster().execute(RefreshFilterAction.INSTANCE, refreshFilterRequest).actionGet();
        if (response != null) {
            for (TransportRefreshFilterAction.NodeRefreshFilterResponse nodeResponse: response.getNodes()) {
                if (nodeResponse.getFailure() != null) {

                }
                assert (nodeResponse.getStatus().length == 1);
            }
        }

        String[] tokens2 = {"the", "fastest", "brown", "fox", "jumped", "over", "the", "sleepy", "dog"};
        //assertTokenStream(tokenStream, tokens2);
    }

    private List<AnalyzeResponse.AnalyzeToken> analyzeText(String index, String analyzer, String text) {
        Client client = node.client();

        AnalyzeRequest analyzeRequest = new AnalyzeRequest(index, text);
        analyzeRequest.listenerThreaded(false);
        analyzeRequest.preferLocal(analyzeRequest.preferLocalShard());
        analyzeRequest.analyzer(analyzer);
        AnalyzeResponse analyzeResponse = client.admin().indices().analyze(analyzeRequest).actionGet();
        return analyzeResponse.getTokens();
    }

    private void assertTokenStream(TokenStream stream, String[] tokens) {
        try {
            stream.reset();
            CharTermAttribute termAttr = stream.getAttribute(CharTermAttribute.class);
            assertNotNull(termAttr);
            int i = 0;
            while (stream.incrementToken()) {
                assertTrue(i < tokens.length);
                assertEquals(tokens[i++], termAttr.toString(), "expected different term at index " + i);
            }
            assertEquals(i, tokens.length, "not all tokens produced");
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createIndex(String name, Settings settings) {
        Client client = node.client();
        client.admin().indices().prepareCreate(name).setSettings(settings).execute().actionGet();
        client.admin().cluster().prepareHealth().setWaitForEvents(Priority.LANGUID).setWaitForGreenStatus().execute().actionGet();
    }

    private void writeFile(File file, List<String> lines) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        for (String line: lines) {
            writer.println(line);
        }
        writer.close();
    }
}