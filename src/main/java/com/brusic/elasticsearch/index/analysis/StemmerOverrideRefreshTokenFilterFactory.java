package com.brusic.elasticsearch.index.analysis;

import com.brusic.elasticsearch.action.refreshfilter.RefreshStatus;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.miscellaneous.StemmerOverrideFilter;
import org.apache.lucene.analysis.miscellaneous.StemmerOverrideFilter.StemmerOverrideMap;
import org.elasticsearch.ElasticSearchIllegalArgumentException;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.analysis.Analysis;
import org.elasticsearch.index.analysis.AnalysisSettingsRequired;
import org.elasticsearch.index.settings.IndexSettings;

import java.util.*;

@AnalysisSettingsRequired
public class StemmerOverrideRefreshTokenFilterFactory extends AbstractRefreshTokenFilterFactory {

	private StemmerOverrideMap overrideMap;

	@Inject
	public StemmerOverrideRefreshTokenFilterFactory(Index index, @IndexSettings Settings indexSettings, Environment env, @Assisted String name, @Assisted Settings settings) throws Exception {
		super(index, indexSettings, name, settings, env);

        refresh();
	}

	@Override
	public TokenStream create(TokenStream tokenStream) {
		return new StemmerOverrideFilter(tokenStream, overrideMap);
	}

	@Override
	public RefreshStatus refresh() throws Exception {
		logger.debug(String.format("Build [%s:%s:%s] ", getSettings().get("type"), name(), index().getName()));

        List<String> rules = Analysis.getWordList(getEnvironment(), getSettings(), "rules");
        if (rules == null) {
            logger.warn("Attempting to load null/empty set");
            throw new ElasticSearchIllegalArgumentException("stemmer override filter requires either `rules` or `rules_path` to be configured");
        }

        StemmerOverrideFilter.Builder builder = new StemmerOverrideFilter.Builder(false);
        parseRules(rules, builder, "=>");
        overrideMap = builder.build();

        String msg = String.format("%s for index %s reloaded %s entries", name(), index().getName(), rules.size());
        logger.debug(msg);
        return new RefreshStatus(msg);
	}

	private void parseRules(List<String> rules, StemmerOverrideFilter.Builder builder, String mappingSep) {
		for (String rule : rules) {
			String key, override;
			List<String> mapping = Strings.splitSmart(rule, mappingSep, false);
			if (mapping.size() == 2) {
				key = mapping.get(0).trim();
				override = mapping.get(1).trim();
			} else {
				throw new RuntimeException("Invalid Keyword override Rule:" + rule);
			}

			if (key.isEmpty() || override.isEmpty()) {
				throw new RuntimeException("Invalid Keyword override Rule:" + rule);
			} else {
				builder.add(key, override);
			}
		}
	}
}
