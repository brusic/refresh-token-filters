package com.brusic.elasticsearch.index.analysis;

import org.elasticsearch.index.analysis.AnalysisModule;

public class RefreshAnalysisBinderProcessor extends AnalysisModule.AnalysisBinderProcessor {

    @Override
    public void processTokenFilters(TokenFiltersBindings tokenFiltersBindings) {
		tokenFiltersBindings.processTokenFilter("synonym_refresh", SynonymRefreshTokenFilterFactory.class);
		tokenFiltersBindings.processTokenFilter("stop_refresh", StopRefreshTokenFilterFactory.class);
		tokenFiltersBindings.processTokenFilter("keyword_marker_refresh", KeywordMarkerRefreshTokenFilterFactory.class);
		tokenFiltersBindings.processTokenFilter("stemmer_override_refresh", StemmerOverrideRefreshTokenFilterFactory.class);
    }
}