package com.brusic.elasticsearch.index.analysis;

import com.brusic.elasticsearch.action.refreshfilter.RefreshStatus;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.synonym.SolrSynonymParser;
import org.apache.lucene.analysis.synonym.SynonymFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.synonym.WordnetSynonymParser;
import org.elasticsearch.ElasticSearchIllegalArgumentException;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.io.FastStringReader;
import org.elasticsearch.common.lucene.Lucene;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.analysis.*;
import org.elasticsearch.index.settings.IndexSettings;
import org.elasticsearch.indices.analysis.IndicesAnalysisService;

import java.io.Reader;
import java.util.*;

@AnalysisSettingsRequired
public class SynonymRefreshTokenFilterFactory extends AbstractRefreshTokenFilterFactory {

    private SynonymMap synonymMap;
    private final boolean ignoreCase;
	private boolean expand;
	private Analyzer analyzer;

    @Inject
	public SynonymRefreshTokenFilterFactory(Index index, @IndexSettings Settings indexSettings, Environment env, IndicesAnalysisService indicesAnalysisService, Map<String, TokenizerFactoryFactory> tokenizerFactories,
                                            @Assisted String name, final @Assisted Settings settings) {
        super(index, indexSettings, name, settings, env);

        this.ignoreCase = settings.getAsBoolean("ignore_case", false);
        this.expand = settings.getAsBoolean("expand", true);

        String tokenizerName = settings.get("tokenizer", "whitespace");

        TokenizerFactoryFactory tokenizerFactoryFactory = tokenizerFactories.get(tokenizerName);
        if (tokenizerFactoryFactory == null) {
            tokenizerFactoryFactory = indicesAnalysisService.tokenizerFactoryFactory(tokenizerName);
        }

        if (tokenizerFactoryFactory == null) {
            throw new ElasticSearchIllegalArgumentException("failed to find tokenizer [" + tokenizerName + "] for synonym token filter");
        }
        final TokenizerFactory tokenizerFactory = tokenizerFactoryFactory.create(tokenizerName, settings);

        this.analyzer = new Analyzer() {
            @Override
            protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
                Tokenizer tokenizer = tokenizerFactory == null ? new WhitespaceTokenizer(Lucene.ANALYZER_VERSION, reader) : tokenizerFactory.create(reader);
                TokenStream stream = ignoreCase ? new LowerCaseFilter(Lucene.ANALYZER_VERSION, tokenizer) : tokenizer;
                return new TokenStreamComponents(tokenizer, stream);
            }
        };

		refresh();
    }

    @Override
	public RefreshStatus refresh() {
		logger.debug(String.format("Build synonyms: [%s:%s:%s] ", getSettings().get("type"), name(), index().getName()));

        Reader rulesReader;

        if (getSettings().getAsArray("synonyms", null) != null) {
            List<String> rules = Analysis.getWordList(getEnvironment(), getSettings(), "synonyms");
            StringBuilder sb = new StringBuilder();
            for (String line : rules) {
                sb.append(line).append(System.getProperty("line.separator"));
            }
            rulesReader = new FastStringReader(sb.toString());
        } else if (getSettings().get("synonyms_path") != null) {
            rulesReader = Analysis.getReaderFromFile(getEnvironment(), getSettings(), "synonyms_path");
        } else {
            throw new ElasticSearchIllegalArgumentException("synonym requires either `synonyms` or `synonyms_path` to be configured");
        }

        try {
            SynonymMap.Builder parser = null;

            if ("wordnet".equalsIgnoreCase(getSettings().get("format"))) {
                parser = new WordnetSynonymParser(true, expand, analyzer);
                ((WordnetSynonymParser) parser).parse(rulesReader);
            } else {
                parser = new SolrSynonymParser(true, expand, analyzer);
                ((SolrSynonymParser) parser).parse(rulesReader);
            }

            synonymMap = parser.build();
        } catch (Exception e) {
            throw new ElasticSearchIllegalArgumentException("failed to build synonyms", e);
        }

		String msg = String.format("%s for index %s reloaded %s entries", name(), index().getName(), synonymMap.words.size());
		logger.debug(msg);
		return new RefreshStatus(msg);
	}

    public int test() {
        return synonymMap.words.size();
    }

	@Override
    public TokenStream
    create(TokenStream tokenStream) {
        // fst is null means no synonyms
        return synonymMap.fst == null ? tokenStream : new SynonymFilter(tokenStream, synonymMap, ignoreCase);
    }
}

