package com.brusic.elasticsearch.index.analysis;

import com.brusic.elasticsearch.action.refreshfilter.RefreshStatus;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.elasticsearch.ElasticSearchIllegalArgumentException;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.analysis.Analysis;
import org.elasticsearch.index.analysis.AnalysisSettingsRequired;
import org.elasticsearch.index.settings.IndexSettings;

import java.util.Set;

@AnalysisSettingsRequired
public class KeywordMarkerRefreshTokenFilterFactory extends AbstractRefreshTokenFilterFactory {

    private Settings settings;

    private CharArraySet keywordLookup;
	boolean ignoreCase;

    @Inject
    public KeywordMarkerRefreshTokenFilterFactory(Index index, @IndexSettings Settings indexSettings, Environment env, @Assisted String name, @Assisted Settings settings) {
        super(index, indexSettings, name, settings, env);
        this.settings = settings;
        this.ignoreCase = settings.getAsBoolean("ignore_case", false);

		refresh();
    }

    @Override
	public RefreshStatus refresh() {
		logger.debug(String.format("Build [%s:%s:%s] ", getSettings().get("type"), name(), index().getName()));

        Set<?> rules = Analysis.getWordSet(getEnvironment(), settings, "keywords", version);
        if (rules == null) {
            throw new ElasticSearchIllegalArgumentException("keyword filter requires either `keywords` or `keywords_path` to be configured");
        }
        keywordLookup = new CharArraySet(version, rules, ignoreCase);

		String msg = String.format("%s for index %s reloaded %s entries", name(), index().getName(), keywordLookup.size());
		logger.debug(msg);
		return new RefreshStatus(msg);
	}

	@Override
    public TokenStream create(TokenStream tokenStream) {
        return new SetKeywordMarkerFilter(tokenStream, keywordLookup);
    }
}
