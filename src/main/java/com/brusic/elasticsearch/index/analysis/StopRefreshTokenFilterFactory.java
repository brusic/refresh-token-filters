package com.brusic.elasticsearch.index.analysis;

import com.brusic.elasticsearch.action.refreshfilter.RefreshStatus;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.search.suggest.analyzing.SuggestStopFilter;
import org.apache.lucene.util.Version;
import org.elasticsearch.ElasticSearchIllegalArgumentException;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.analysis.Analysis;
import org.elasticsearch.index.analysis.AnalysisSettingsRequired;
import org.elasticsearch.index.settings.IndexSettings;

/**
 *
 */
@AnalysisSettingsRequired
@SuppressWarnings("deprecation")
public class StopRefreshTokenFilterFactory extends AbstractRefreshTokenFilterFactory {

    private Environment env;

    private CharArraySet stopWords;

    private boolean ignoreCase;

    private boolean enablePositionIncrements;
    private final boolean removeTrailing;

    @Inject
    public StopRefreshTokenFilterFactory(Index index, @IndexSettings Settings indexSettings, Environment env, @Assisted String name, @Assisted Settings settings) {
        super(index, indexSettings, name, settings, env);
        this.env = env;

		this.ignoreCase = settings.getAsBoolean("ignore_case", false);
        this.removeTrailing = settings.getAsBoolean("remove_trailing", true);
        this.enablePositionIncrements = settings.getAsBoolean("enable_position_increments", true);
        if (!enablePositionIncrements && version.onOrAfter(Version.LUCENE_44)) {
            throw new ElasticSearchIllegalArgumentException("[enable_position_increments: false] is not supported anymore as of Lucene 4.4 as it can create broken token streams."
                    + " Please fix your analysis chain or use an older compatibility version (<=4.3) but beware that it might cause unexpected behavior.");
        }

		refresh();
	}

    @Override
    public TokenStream create(TokenStream tokenStream) {
        if (removeTrailing) {
            StopFilter filter = new StopFilter(version, tokenStream, stopWords);
            filter.setEnablePositionIncrements(enablePositionIncrements);
            return filter;
        } else {
            return new SuggestStopFilter(tokenStream, stopWords);
        }
    }

    @Override
	public RefreshStatus refresh() {
        this.stopWords = Analysis.parseStopWords(env, getSettings(), StopAnalyzer.ENGLISH_STOP_WORDS_SET, version, ignoreCase);

		String msg = String.format("%s for index %s reloaded %s entries", name(), index().getName(), stopWords.size());
		logger.debug(msg);
		return new RefreshStatus(msg);
	}
}
