package com.brusic.elasticsearch.index.analysis;

import com.brusic.elasticsearch.action.refreshfilter.RefreshStatus;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.analysis.AbstractTokenFilterFactory;
import org.elasticsearch.index.settings.IndexSettings;

/**
 * Base class for all refreshable token filters
 */
public abstract class AbstractRefreshTokenFilterFactory extends AbstractTokenFilterFactory {

    private Settings settings;
    private Environment env;

    public abstract RefreshStatus refresh() throws Exception;

    public AbstractRefreshTokenFilterFactory(Index index, @IndexSettings Settings indexSettings, String name, Settings settings, Environment env) {
        super(index, indexSettings, name, settings);
        this.settings = settings;
        this.env = env;

        logger.debug("Creating " + name);
    }

    Settings getSettings() {
        return settings;
    }

    public Environment getEnvironment() {
        return env;
    }
}
