package com.brusic.elasticsearch.rest.refreshfilter;

import org.elasticsearch.action.ActionListener;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterAction;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterRequest;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.XContentRestResponse;
import org.elasticsearch.rest.XContentThrowableRestResponse;
import org.elasticsearch.rest.action.support.RestActions;
import org.elasticsearch.rest.action.support.RestXContentBuilder;

import java.io.IOException;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestRequest.Method.POST;
import static org.elasticsearch.rest.RestStatus.OK;

public class RestRefreshFilterAction extends BaseRestHandler {

    @Inject
    public RestRefreshFilterAction(Settings settings, Client client, RestController controller) {
        super(settings, client);

        controller.registerHandler(GET, "/_refreshfilter/{type}", this);
        controller.registerHandler(POST, "/_refreshfilter/{type}", this);
        controller.registerHandler(GET, "/{index}/_refreshfilter/{type}", this);
        controller.registerHandler(POST, "/{index}/_refreshfilter/{type}", this);
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel) {
        String[] indices = RestActions.splitIndices(request.param("index"));

        RefreshFilterRequest refreshFilterRequest = new RefreshFilterRequest();
        refreshFilterRequest.indices(indices);
        refreshFilterRequest.tokenFilter(request.param("type"));

        refreshFilterRequest.listenerThreaded(false);

        client.admin().cluster().execute(RefreshFilterAction.INSTANCE, refreshFilterRequest, new ActionListener<RefreshFilterResponse>() {
            @Override
            public void onResponse(RefreshFilterResponse refreshFilterResponse) {
                try {
                    XContentBuilder builder = RestXContentBuilder.restContentBuilder(request);
                    builder.startObject();
                    refreshFilterResponse.toXContent(builder, request);
                    builder.endObject();
                    channel.sendResponse(new XContentRestResponse(request, OK, builder));
                } catch (Exception e) {
                    onFailure(e);
                }
            }

            @Override
            public void onFailure(Throwable e) {
                try {
                    channel.sendResponse(new XContentThrowableRestResponse(request, e));
                } catch (IOException ioException) {
                    logger.error("Failed to send failure response", ioException);
                }
            }
        });
    }
}