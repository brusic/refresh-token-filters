package com.brusic.elasticsearch.plugin.analysis;

import com.brusic.elasticsearch.rest.refreshfilter.RestRefreshFilterAction;
import com.brusic.elasticsearch.index.analysis.*;
import org.elasticsearch.action.ActionModule;
import com.brusic.elasticsearch.action.refreshfilter.RefreshFilterAction;
import com.brusic.elasticsearch.action.refreshfilter.TransportRefreshFilterAction;
import org.elasticsearch.index.analysis.AnalysisModule;
import org.elasticsearch.plugins.AbstractPlugin;
import org.elasticsearch.rest.RestModule;

public class RefreshFiltersPlugin extends AbstractPlugin {

     public String name() {
       return "refresh-token-filters";
     }

     public String description() {
		 return "Refreshable Token Filters";
	 }

	public void onModule(ActionModule module) {
		module.registerAction(RefreshFilterAction.INSTANCE, TransportRefreshFilterAction.class);
	}

	public void onModule(AnalysisModule module) {
		module.addProcessor(new RefreshAnalysisBinderProcessor());
	}

    public void onModule(RestModule module) {
        module.addRestAction(RestRefreshFilterAction.class);
    }
}
