package com.brusic.elasticsearch.action.refreshfilter;

import org.elasticsearch.action.support.nodes.NodesOperationResponse;
import org.elasticsearch.cluster.ClusterName;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;

public class RefreshFilterResponse extends NodesOperationResponse<TransportRefreshFilterAction.NodeRefreshFilterResponse> implements ToXContent {

	RefreshFilterResponse() {}

	public RefreshFilterResponse(ClusterName clusterName, TransportRefreshFilterAction.NodeRefreshFilterResponse[] nodes) {
		super(clusterName, nodes);
	}

	@Override
	public void readFrom(StreamInput in) throws IOException {
		super.readFrom(in);
		nodes = new TransportRefreshFilterAction.NodeRefreshFilterResponse[in.readVInt()];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = TransportRefreshFilterAction.NodeRefreshFilterResponse.readNodeResfreshFilterResponse(in);
		}
	}

	@Override
	public void writeTo(StreamOutput out) throws IOException {
		super.writeTo(out);
		out.writeVInt(nodes.length);
		for (TransportRefreshFilterAction.NodeRefreshFilterResponse node : nodes) {
			node.writeTo(out);
		}
	}

	@Override
	public XContentBuilder toXContent(XContentBuilder builder, Params params) throws IOException {
		builder.startObject("nodes");
		for (TransportRefreshFilterAction.NodeRefreshFilterResponse nodeRefreshFilterResponse : nodes) {
			builder.startObject(nodeRefreshFilterResponse.getNode().id(), XContentBuilder.FieldCaseConversion.NONE);

			String[] status = nodeRefreshFilterResponse.getStatus();
			if (status != null) {
				builder.array("status", status);
			}

			String[] failure = nodeRefreshFilterResponse.getFailure();
			if (failure != null) {
				builder.array("failure", failure);
			}
			builder.endObject();
		}
		builder.endObject();

		return builder;
	}
}