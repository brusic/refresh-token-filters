package com.brusic.elasticsearch.action.refreshfilter;

import com.brusic.elasticsearch.index.analysis.AbstractRefreshTokenFilterFactory;
import org.elasticsearch.ElasticSearchException;
import org.elasticsearch.action.support.IgnoreIndices;
import org.elasticsearch.action.support.nodes.NodeOperationRequest;
import org.elasticsearch.action.support.nodes.NodeOperationResponse;
import org.elasticsearch.action.support.nodes.TransportNodesOperationAction;
import org.elasticsearch.cluster.ClusterName;
import org.elasticsearch.cluster.ClusterService;
import org.elasticsearch.cluster.ClusterState;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.Nullable;
import org.elasticsearch.common.collect.Lists;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.analysis.*;
import org.elasticsearch.index.service.IndexService;
import org.elasticsearch.indices.IndicesService;
import org.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.transport.TransportService;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class TransportRefreshFilterAction extends TransportNodesOperationAction<RefreshFilterRequest, RefreshFilterResponse, TransportRefreshFilterAction.NodeRefreshRequest, TransportRefreshFilterAction.NodeRefreshFilterResponse> {

    private final IndicesService indicesService;

	@Inject
	public TransportRefreshFilterAction(Settings settings, ClusterName clusterName, ThreadPool threadPool, ClusterService clusterService,
                                        TransportService transportService, IndicesService indicesService) {
		super(settings, clusterName, threadPool, clusterService, transportService);
        this.indicesService = indicesService;
	}

	@Override
	protected NodeRefreshFilterResponse newNodeResponse() {
		return new NodeRefreshFilterResponse();
	}

	@Override
	protected NodeRefreshFilterResponse nodeOperation(NodeRefreshRequest nodeRefreshRequest) throws ElasticSearchException {
        String type = nodeRefreshRequest.tokenFilter();

        IndexService indexService;
        List<String> success = Lists.newArrayList();
        List<String> failure = Lists.newArrayList();

        ClusterState clusterState = clusterService.state();
        String[] concreteIndices = clusterState.metaData().concreteIndices(nodeRefreshRequest.indices(), IgnoreIndices.DEFAULT, true);

        // iterate through all indices requested and use its AnalysisService to lookup the token filter
        for (String index: concreteIndices) {
            indexService = indicesService.indexServiceSafe(index);
            TokenFilterFactory factory = indexService.analysisService().tokenFilter(type);
            if (factory instanceof AbstractRefreshTokenFilterFactory) {
                try {
                    RefreshStatus refreshStatus = ((AbstractRefreshTokenFilterFactory) factory).refresh();
                    success.add(refreshStatus.getStatus());
                } catch (Exception e) {
                    failure.add(e.getMessage());
                    logger.error("Could not refresh " + type + " for index " + index, e);
                }
            }
        }

		return new NodeRefreshFilterResponse(clusterService.state().nodes().localNode(), success.toArray(new String[success.size()]), failure.toArray(new String[failure.size()]));
	}


    @Override
	protected NodeRefreshRequest newNodeRequest() {
		return new NodeRefreshRequest();
	}

	@Override
	protected NodeRefreshRequest newNodeRequest(String nodeId, RefreshFilterRequest request) {
		return new NodeRefreshRequest(nodeId, request);
	}

	@Override
	protected boolean accumulateExceptions() {
		return false;
	}

	@Override
	protected RefreshFilterResponse newResponse(RefreshFilterRequest nodesInfoRequest, AtomicReferenceArray responses) {
		final List<NodeRefreshFilterResponse> nodeRefreshFilterResponses = Lists.newArrayList();
		for (int i = 0; i < responses.length(); i++) {
			Object resp = responses.get(i);

			if (resp instanceof NodeRefreshFilterResponse) {
				nodeRefreshFilterResponses.add((NodeRefreshFilterResponse) resp);
			}
		}
		return new RefreshFilterResponse(clusterName, nodeRefreshFilterResponses.toArray(new NodeRefreshFilterResponse[nodeRefreshFilterResponses.size()]));
	}

	@Override
	protected String transportAction() {
		return RefreshFilterAction.NAME;
	}

	@Override
	protected String executor() {
		return ThreadPool.Names.GENERIC;
	}

	@Override
	protected RefreshFilterRequest newRequest() {
		return new RefreshFilterRequest();
	}

	static class NodeRefreshRequest extends NodeOperationRequest {
        String[] indices;
		String tokenFilter;

		NodeRefreshRequest() {}

		NodeRefreshRequest(String nodeId, RefreshFilterRequest request) {
			super(request, nodeId);
            this.indices = request.indices();
			this.tokenFilter = request.tokenFilter();
		}

		@Override
		public void readFrom(StreamInput in) throws IOException {
			super.readFrom(in);
			tokenFilter = in.readString();
		}

		@Override
		public void writeTo(StreamOutput out) throws IOException {
			super.writeTo(out);
			out.writeString(tokenFilter);
		}

        public String tokenFilter() {
            return tokenFilter;
        }

        public String[] indices() {
            return indices;
        }
    }

	public static class NodeRefreshFilterResponse extends NodeOperationResponse {

		String[] status;
		String[] failure;

		NodeRefreshFilterResponse() {
			super();
		}

		NodeRefreshFilterResponse(DiscoveryNode node, String[] success, String[] failure) {
			super(node);
			this.status = success;
			this.failure = failure;
		}

		@Nullable
		public String[] getStatus() {
			return status;
		}

		@Nullable
		public String[] getFailure() {
			return failure;
		}

		public static NodeRefreshFilterResponse readNodeResfreshFilterResponse(StreamInput in) throws IOException {
			NodeRefreshFilterResponse response = new NodeRefreshFilterResponse();
			response.readFrom(in);
			return response;
		}

		@Override
		public void readFrom(StreamInput in) throws IOException {
			super.readFrom(in);
			if (in.readBoolean()) {
				status = in.readStringArray();
			}
			if (in.readBoolean()) {
				failure = in.readStringArray();
			}
		}

		@Override
		public void writeTo(StreamOutput out) throws IOException {
			super.writeTo(out);
			if (status == null) {
				out.writeBoolean(false);
			} else {
				out.writeBoolean(true);
				out.writeStringArray(status);
			}
			if (failure == null) {
				out.writeBoolean(false);
			} else {
				out.writeBoolean(true);
				out.writeStringArray(failure);
			}
		}
	}
}
