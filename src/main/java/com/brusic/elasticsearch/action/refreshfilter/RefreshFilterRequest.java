package com.brusic.elasticsearch.action.refreshfilter;

import org.elasticsearch.ElasticSearchIllegalArgumentException;
import org.elasticsearch.action.support.nodes.NodesOperationRequest;
import org.elasticsearch.common.Nullable;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;

import java.io.IOException;

public class RefreshFilterRequest extends NodesOperationRequest<RefreshFilterRequest> {

	public static String ALL = "_ALL_";

    private String[] indices;
	private String tokenFilter;

    public RefreshFilterRequest() {
		super();
    }

    /**
     * Sets the indices the search will be executed on.
     */
    public RefreshFilterRequest indices(@Nullable String... indices) {
        if (indices == null) {

        } else {
            for (int i = 0; i < indices.length; i++) {
                if (indices[i] == null) {
                    throw new ElasticSearchIllegalArgumentException("indices[" + i +"] must not be null");
                }
            }
        }

        this.indices = indices;
        return this;
    }

    @Override
     public void readFrom(StreamInput in) throws IOException {
        super.readFrom(in);
        indices = new String[in.readVInt()];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = in.readString();
        }
		tokenFilter = in.readString();
      }

    @Override
     public void writeTo(StreamOutput out) throws IOException {
        super.writeTo(out);
        out.writeVInt(indices.length);
        for (String index : indices) {
            out.writeString(index);
        }
        out.writeString(tokenFilter);
     }

	public String tokenFilter() {
		return tokenFilter;
    }

	public RefreshFilterRequest tokenFilter(String tokenFilter) {
		// do not allow null strings due to serialization
		// null == ALL
		this.tokenFilter = tokenFilter != null ? tokenFilter : ALL;
        return this;
	}

    public String[] indices() {
        return this.indices;
    }
}