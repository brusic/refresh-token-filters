package com.brusic.elasticsearch.action.refreshfilter;

public class RefreshStatus {

	private String status;

    public RefreshStatus(String status) {
        this.status = status;
    }

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return status;
	}
}