package com.brusic.elasticsearch.action.refreshfilter;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.support.nodes.NodesOperationRequestBuilder;
import org.elasticsearch.client.ClusterAdminClient;
import org.elasticsearch.client.internal.InternalClusterAdminClient;

public class RefreshFilterRequestBuilder extends NodesOperationRequestBuilder<RefreshFilterRequest, RefreshFilterResponse, RefreshFilterRequestBuilder> {

    public RefreshFilterRequestBuilder(ClusterAdminClient clusterClient) {
        super((InternalClusterAdminClient) clusterClient, new RefreshFilterRequest());
    }

    @Override
    protected void doExecute(ActionListener<RefreshFilterResponse> listener) {
		((ClusterAdminClient) client).execute(RefreshFilterAction.INSTANCE, request, listener);
    }
}