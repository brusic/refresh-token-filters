package com.brusic.elasticsearch.action.refreshfilter;

import org.elasticsearch.action.admin.cluster.ClusterAction;
import org.elasticsearch.client.ClusterAdminClient;

public class RefreshFilterAction extends ClusterAction<RefreshFilterRequest, RefreshFilterResponse, RefreshFilterRequestBuilder> {

    public static final RefreshFilterAction INSTANCE = new RefreshFilterAction();
    public static final String NAME = "indices/RefreshFilter";

    private RefreshFilterAction() {
        super(NAME);
    }

    @Override
    public RefreshFilterResponse newResponse() {
        return new RefreshFilterResponse();
    }

    @Override
    public RefreshFilterRequestBuilder newRequestBuilder(ClusterAdminClient client) {
        return new RefreshFilterRequestBuilder(client);
    }
}