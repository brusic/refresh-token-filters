Reboot of a previous refresh filter plugin.

Simplifies the code by only focusing on explicit refreshes and only uses files. No database loading and no automatic
 reloading with timer threads.

Uses the IndicesService to retrieve the token filters instead of using an internal JVM-level data structure. Because of
this change, caching seems to be an issue in some cases. To be resolved.